<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files)
    {
        try {
            // Array que vai ser retornado com os Arquivos organizados pelos Donos
            $newFiles = array();
            
            foreach ($files as $file => $f) {
                // Tranformando a palavra em minuscula e a primeira letra maisúscula
                $dono = ucfirst(strtolower($f));

                // Array aux que armazena os arquivos
                $newArray = array();
                if (empty($newFiles[$dono])) {
                    // Caso o Dono não tenha Arquivo associado, adicionar o novo arquivo em um array e associar ao Dono
                    $newArray[] = $file;
                    $newFiles[$dono] = $newArray;
                } else {
                    // Caso o Dono já tenha Arquivo associado, copiar um array com o arquivo antigo e adicionar o arquivo novo e associar ao Dono
                    $newArray = $newFiles[$dono];
                    $newArray[] = $file;
                    $newFiles[$dono] = $newArray;
                }
            }

            return $newFiles;
        } catch (Exception $e) {
            throw $e;
        }
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",

);
var_dump(FileOwners::groupByOwners($files));