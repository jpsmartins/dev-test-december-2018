<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/




class Palindrome
{
    public static function isPalindrome($word)
    {
        // Transformando as letras em maiúsculo para que não haja problemas
        $word = strtoupper($word);

        // Criando variável com a palavra recebida invertida 
        $wordReversed = strrev($word);

        // Comparando a palavra recebida com a invertida
        if ($word == $wordReversed) return true;
        
        return false;
    }
}

echo Palindrome::isPalindrome('Asa');