<?php
/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

A classe LeagueTablr acompanha o score de cada jogador em uma liga. Depois de cada jogo, o score do jogador é salvo utilizanod a função recordResult.

O Rank de jogar na liga é calculado utilizando a seguinte lógica:

1- O jogador com a pontuação mais alta fica em primeiro lugar. O jogador com a pontuação mais baixa fica em último.
2- Se dois jogadores estiverem empatados, o jogador que jogou menos jogos é melhor posicionado.
3- Se dois jogadores estiverem empatados na pontuação e no número de jogos disputados, então o jogador que foi o primeiro na lista de jogadores é classificado mais alto.


Implemente a funação playerRank que retorna o jogador de uma posição escolhida do ranking.

Exemplo:

$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 2);
$table->recordResult('Mike', 3);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);


Todos os jogadores têm a mesma pontuação. No entanto, Arnold e Chris jogaram menos jogos do que Mike, e como Chris está acima de Arnold na lista de jogadores, ele está em primeiro lugar.

Portanto, o código acima deve exibir "Chris".


*/

class LeagueTable
{
	public function __construct($players)
    {
		$this->standings = array();
		foreach($players as $index => $p)
        {
			$this->standings[$p] = array
            (
                'index' => $index,
                'games_played' => 0,
                'score' => 0
            );
        }
	}

	public function recordResult($player, $score)
    {
		$this->standings[$player]['games_played']++;
		$this->standings[$player]['score'] += $score;
    }

    // Método que troca os Jogadores de posição em um array
    // (recebe o array e o nome dos Jogadores a serem trocados [keys do array])
    public function swapArrayElements($originalArray, $playerOne, $playerTwo) {

        // Criando um novo array com as posições trocadas
        $auxArray = array();
        foreach ($originalArray as $player=>$data) {

            // Trocando a posição do Jogador 1 pelo 2
            if ($player == $playerOne) {
                $auxArray[$playerTwo] = $originalArray[$playerTwo];
            } else {

                // Trocando a posição do Jogador 2 pelo 1
                if ($player == $playerTwo) {
                    $auxArray[$playerOne] = $originalArray[$playerOne];
                }
                // Quando for um Jogador que não necessite trocar a posição
                else {
                    $auxArray[$player] = $data;
                }
            }
        }

        return $auxArray;
    }

    // Método que retorna o array de Jogadores e Dados recebido,
    // ordenado de acordo com as regras do campeonato
    public function sortStandings($currentPlayersRanking) {

        // Vou fazer um Selection Sort onde eu pego sempre, neste caso, os Jogadores em ordem descrescente
        // Logo, de todos os Jogadores vou pegar primeiro o 1º, depois o 2º, 3º e assim sucessivamente
        // E vou adicionar em um novo array, ao final se for o caso pode-se alterar o array da classe
        for ($i = 0; $i < count($currentPlayersRanking); $i++) {

            // Criando um array com chaves numericas dos nomes dos Jogadores
            $topAllPlayers = array_keys($currentPlayersRanking);

            // Dados do jogador em melhor posição
            $topPlayer = $topAllPlayers[$i];
            $topIndex = $currentPlayersRanking[$topAllPlayers[$i]]["index"];
            $topGamesPlayed = $currentPlayersRanking[$topAllPlayers[$i]]["games_played"];
            $topScore = $currentPlayersRanking[$topAllPlayers[$i]]["score"];

            for ($j = $i + 1; $j < count($currentPlayersRanking); $j++) {

                // Criando um array com chaves numericas dos nomes dos Jogadores
                $currentAllPlayers = array_keys($currentPlayersRanking);

                // Dados do jogador que está sendo analizado
                $currentPlayer = $currentAllPlayers[$j];
                $currentIndex = $currentPlayersRanking[$currentAllPlayers[$j]]["index"];
                $currentGamesPlayed = $currentPlayersRanking[$currentAllPlayers[$j]]["games_played"];
                $currentScore = $currentPlayersRanking[$currentAllPlayers[$j]]["score"];

                // Porque não quero comparar a pontuação dos mesmos Jogadores
                if ($topPlayer != $currentPlayer) {

                    // Caso a pontuação do jogador atual seja maior
                    if ($topScore < $currentScore) {

                        // Trocando os Jogadores de posição no array
                        $currentPlayersRanking = $this->swapArrayElements($currentPlayersRanking, $topPlayer, $currentPlayer);

                        // Guardo os dados do Jogador com a maior pontuação
                        $topPlayer = $currentPlayer;
                        $topIndex = $currentIndex;
                        $topGamesPlayed = $currentGamesPlayed;
                        $topScore = $currentScore;

                    }

                    // Caso a pontuação seja igual
                    if ($topScore == $currentScore) {

                        // Caso a quantidade de jogos jogados do jogador atual seja menor
                        if ($topGamesPlayed > $currentGamesPlayed) {

                            // Trocando os Jogadores de posição no array
                            $currentPlayersRanking = $this->swapArrayElements($currentPlayersRanking, $topPlayer, $currentPlayer);

                            // Guardo os dados do Jogador com a menor qtd de jogos jogados
                            $topPlayer = $currentPlayer;
                            $topIndex = $currentIndex;
                            $topGamesPlayed = $currentGamesPlayed;
                            $topScore = $currentScore;
                        }

                        // Caso a qtd de jogos jogados seja igual
                        if ($topGamesPlayed == $currentGamesPlayed) {

                            // Então comparar o menor Index do Jogador
                            if ($topIndex > $currentIndex) {

                                // Trocando os Jogadores de posição no array
                                $currentPlayersRanking = $this->swapArrayElements($currentPlayersRanking, $topPlayer, $currentPlayer);

                                // Guardo os dados do Jogador com o menor Index
                                $topPlayer = $currentPlayer;
                                $topIndex = $currentIndex;
                                $topGamesPlayed = $currentGamesPlayed;
                                $topScore = $currentScore;
                            }
                        }
                    }
                }
            }
        }

        return $currentPlayersRanking;
    }

	public function playerRank($rank)
    {
        // Validando o rank pedido
        if ($rank <= 0 || empty($rank) || !is_numeric($rank)){
            return "Erro - Digite um rank válido!";
        }

        // Validando se existem jogadores
        if (empty($this->standings)) {
            return "Erro - Não existem jogadores!";
        }

        // Validando se o rank está dentro da quantidade de jogadores
        if ($rank > count($this->standings) ) {
            return "Erro - O rank escolhido é maior do que a qtd. de Jogadores!";
        }

        // Primeiro vou ordenar os Jogadores de acordo com as condições do campeonato
        $sortedStandings = $this->sortStandings($this->standings);

        // Agora retornar o Jogador de acordo com o rank recebido
        // Criando um array com chaves numericas dos nomes dos Jogadores
        // Retornaremos o Nome do Jogador cuja a Key + 1 seja igual ao rank recebido
        $sortedAllPlayers = array_keys($sortedStandings);

        // Iterando o array de jogadores para pegar o Jogador correspondente
        foreach ($sortedAllPlayers as $r=>$player) {

            // Se o rank escolhido for igual ao de algum jogador atualizar a variável
            if ($rank == ($r + 1)) {
                
                return $player;
            }
        }

        return "Erro";
	}
}

$table = new LeagueTable(array('Mike', 'Chris', 'Arnold'));
$table->recordResult('Mike', 3);
$table->recordResult('Mike', 2);
$table->recordResult('Arnold', 5);
$table->recordResult('Chris', 5);
echo $table->playerRank(1);