// Uma galeria de imagens é um conjunto de imagens com botões de remoção correspondentes.
// Este é o código HTML de uma galeria com duas imagens:

// <div class="image">
//   <img src="https://goo.gl/kjzfbE" alt="First">
//   <button class="remove">X</button>
// </div>
// <div class="image">
//   <img src="https://goo.gl/d2JncW" alt="Second">
//   <button class="remove">X</button>
// </div>

// Implemente uma função de configuração que ao receber um evento de click implementa a seguinte lógica:
// * Quando o botão da classe "remove" é clicado, seu elemento div pai deve ser removido da galeria

// Por exemplo, depois que a primeira imagem da galeria acima foi removida, o código HTML ficaria assim:

// <div class="image">
//   <img src="https://goo.gl/d2JncW" alt="Second">
//   <button class="remove">X</button>
// </div>

function setup(id) {
  // Pegando o elemento de div da imagem selecionada
  var divImagem = document.getElementById(id).parentNode;

  // Removendo o elemento selecionado
  divImagem.parentNode.removeChild(divImagem);
}

// Adicionei uma "id" para diferenciar as imagens e passar a "id" para a função
document.body.innerHTML = `
<div class="image">
  <img src="https://goo.gl/kjzfbE" alt="First" id="1">
  <button class="remove" onclick="setup(0)">X</button>
</div>
<div class="image">
  <img src="https://goo.gl/d2JncW" alt="Second" id="2">
  <button class="remove" onclick="setup(1)">X</button>
</div>`;

setup(1);

$(".remove").get(0).click();
console.log(document.body.innerHTML);
