// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.

function formatDate(userDate) {
  // format from M/D/YYYY to YYYYMMDD

  // Validar se é String
  if (typeof userDate === "string") {
    // Transformando a data em um array com 3 elementos (dia, mês e ano)
    let novoUserDate = userDate.split("/");

    // Invertendo a ordem do Array
    novoUserDate.reverse();

    // Juntando o Array em String
    novoUserDate = novoUserDate.join("");

    // Ou simplesmente
    // novoUserDate = userDate.split("/").reverse().join("");

    return novoUserDate;
  } else {
    console.log("Erro - Data não é do tipo texto!");
  }
}

console.log(formatDate("31/12/2014"));
