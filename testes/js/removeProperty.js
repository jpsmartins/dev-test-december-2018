// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {
  // Validando se a função recebe de fato um objeto
  if (typeof obj === "object") {
    // Caso a Propriedade esteja no Objeto
    if (prop in obj) {
      // Removendo a Propriedade do Objeto
      delete obj[prop];

      return true;
    }

    // Caso a Propriedade não esteja no Objeto
    return false;
  } else {
    // Erro - Função não recebeu um objeto
    console.log("Erro - Não é um Objeto!");
    return false;
  }
}

let objeto = {
  Nome: "João Pedro",
  Sobrenome: "Martins",
  Idade: 22,
  Faculdade: "Sistemas de Informação",
};

console.log(removeProperty(objeto, "Nome"));
